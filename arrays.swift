import Foundation

// Создать массив элементов 'кол-во дней в месяцах' содержащих количество дней в соответствующем месяце
let days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

//Создать массив элементов 'название месяцов' содержащий названия месяцев
let months = ["January", "February", "March", "April", "May", "June", "July", "August", 
                "September", "October", "November", "December"]

//Используя цикл for и массив 'кол-во дней в месяцах' выведите количество дней в каждом месяце
for i in days{
    print(i)
}

//Используйте еще один массив с именами месяцев, чтобы вывести название месяца + количество дней
for i in 0..<days.count{
    print("The \(months[i]) has \(days[i]) days.")
}

//Сделайте тоже самое, но используя массив tuples (кортежей) с параметрами (имя месяца, кол-во дней)
let dayMonths = [("January", 31), ("February", 28), ("March", 31), ("April", 30), ("May", 31), ("June", 30), 
                ("July", 31), ("August", 31), ("September", 30), ("October", 31), ("November", 30), ("December", 31)]

for i in dayMonths{
    print(i)
}

//Сделайте тоже самое, только выводите дни в обратном порядке (порядок в массиве не менять)
for i in 0..<dayMonths.count{
    print(dayMonths[11-i])
 }

 //Для произвольно выбранной даты (месяц и день) посчитайте количество дней до этой даты от начала года
let date = ("March", 13)
var (month, day) = date

var numberMonth = 0
for i in 0..<months.count{
    if(months[i] == month){
        numberMonth = i;
    }
}

var dayPassed = 0
for i in 0..<numberMonth{
    dayPassed += days[i]
}
dayPassed += day-1
print(dayPassed)