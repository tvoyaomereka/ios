import Foundation

//Создать класс родитель и 2 класса наследника
class Building{
    var year : Int?
    var floors : Int?
}

class Elevator : Building{
    var elevator : Bool?
}

class Color : Building{
    var color : String?
}

//Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов:
//*create*(выводит площадь),*destroy*(отображает что дом уничтожен)
class House{
    let width : Int
    let height : Int
    
    init(width: Int, height: Int){
        self.width = width
        self.height = height
    }
    
    func create (){
        let area = self.width * self.height
        print("The area of the house is \(area).")
    }
    
    func destroy(){
        print("The house was destroyed.")
    }
}

let firstHouse = House(width: 2, height: 6)
firstHouse.create()
firstHouse.destroy()

//Создайте класс с методами, которые сортируют массив учеников по разным параметрам
class Students{
    let name : String
    let age : Int
    
    init(name: String, age: Int){
        self.name = name
        self.age = age
    }
    
    func sortName(array: inout[Students]){
        var sortedNames = array.sorted(by: {$0.name.count > $1.name.count})
        print("The Students were sorted by names:")
        for i in sortedNames{
            print(i.name, "\(i.age) y.o.")
        }
    }
    func sortAge(array: inout[Students]){
        var sortedAges = array.sorted(by: {$0.age < $1.age})
        print("The Students were sorted by ages:")
        for i in sortedAges{
            print(i.name, "\(i.age) y.o.")
        }
    }
}

let firstStudent = Students(name: "Paul", age: 15)
let secondStudent = Students(name: "Alexa", age: 16)

var arrayOf = [Students]()
arrayOf.append(firstStudent)
arrayOf.append(secondStudent)

firstStudent.sortName(array: &arrayOf)
firstStudent.sortAge(array: &arrayOf)

//Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов
struct RandomStruct{
    let fio : String
    let age : Int
}

class RandomClass{
    let fio : String
    let age: Int
    
    init(fio: String, age: Int){ //Классу необходимо прописать init, у структуры он создается автоматически
        self.fio = fio
        self.age = age
    }
}

/*1. Классу необходимо прописать init, у структуры он создается автоматически
2. Если экземпляр класса - константа, а свойство - переменная, то экземпляру можно переинициализировать это свойство. 
В структуре такое возможно только если и свойство, и экземпляр - переменные
3. 
Структура является Value Type
Константа является Reference Type

var str1 = RandomStruct(fio: "Ivanov Ivan", 14)
var str2 = str1 <--- копирование str1 => при изменении значения экземпляра оно меняется только у него

var cl1 = RandomClass(fio: "Ivanov Ivan", 14)
var cl2 = cl1 <--- ссылка на тот же экземпляр, что и cl1 => при изменении значения оно меняется и там и там
4. Классы имеют наследование
*/

//Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт
//Сохраняйте комбинации в массив
//Если выпала определённая комбинация - выводим соответствующую запись в консоль

//Функция простая, выводит только название комбинации на основе сгенерированных карт и мастей

class PokerGame{
    let combinations = ["Пара", "Две пары", "Сет", "Стрит", "Флэш", "Фулл хаус", 
                        "Карэ", "Стрит флэш", "Флэш рояль"]
                        
    var cardsNum = [Int]()
    var cardsSuit = [Int]()
    
    init(cardsNum: inout[Int], cardsSuit: inout[Int]){
        self.cardsNum = cardsNum
        self.cardsSuit = cardsSuit 
    }
    
    var allCards = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] //массив встречающихся карт
    var allSuits = [0, 0, 0, 0] //массив мастей
    
    func combo(cardsArray: inout[Int], suitsArray: inout[Int]){
        //заполним массив allCards количеством встречающихся карт для упрощения проверки
        var value = 0
        for i in 0...4{
            value = cardsArray[i]
            allCards[value-1] += 1;
        }
        //заполним массив allSuits количеством встречающихся мастей для упрощения проверки
        value = 0
        for i in 0...4{
            value = suitsArray[i]
            allSuits[value-1] += 1;
        }
        var checking = 14 //Для нахождения комбинации среди значений карт
        
        //Флэш
        for i in 0...3{
            if(allSuits[i] == 5){
                checking = 4
            }
        }
        //карэ
        for i in 0...12{
            if(allCards[i] == 4){
                checking = 6
            }
        }
        //пара, две пары, сэт, фулл хаус
        for i in 0...11{
            for j in 1...12{
                if (allCards[i] == 2 && allCards[j] != 2 && allCards[j] != 3){//пара
                    checking = 0
                    break;
                }
                else if(allCards[i] == allCards[j] && allCards[i] == 2){ //Две пары
                    checking += 1
                }
                else if (allCards[i] == 2 && allCards[j] == 3 || allCards[i] == 3 && allCards[j] == 2){ //Фулл хаус
                    checking = 5
                }
                else if(allCards[i] == 3 && allCards[j] != 2 ){ //Сэт
                    checking = 2
                }
            }
        }
        //Стрит и стрит флэш
        for i in 0...8{
            if(allCards[i] == 1 && allCards[i+1] == 1 && allCards[i+2] == 1 && allCards[i+3] == 1 && allCards[i+4] == 1){
                if(checking != 4){
                    checking = 3
                }
                else {
                    checking = 7
                }
            }
        }
        //Роял флэш
        if(allCards[12] == 1 && allCards[11] == 1 && allCards[10] == 1 && allCards[9] == 1 && allCards[8] == 1 && checking == 4){
            checking = 8
        }
        
        
        //вывод комбинации
        if (checking == 14){
            print("У Вас на руках старшая карта")
        }
        else {
            print("У Вас на руках \(combinations[checking])")
        }
    }
    
}

var suitArray = [Int]()//массив мастей

func appendingSuit(array: inout[Int]){ //наполнение мастей
    for i in 1...5{
        array.append(Int.random(in: 1...4))
    }
}
appendingSuit(array: &suitArray)

var cardArray = [Int]()//массив карт, где 1 - туз, 13 - двойка

func appendingCard(array: inout[Int]){ //наполнение карт
    for i in 1...5{
        array.append(Int.random(in: 1...13))
    }
}
appendingCard(array: &cardArray)

let firstGame = PokerGame(cardsNum: &cardArray, cardsSuit: &suitArray)
firstGame.combo(cardsArray: &cardArray, suitsArray: &suitArray)