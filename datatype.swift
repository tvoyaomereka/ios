import Foundation
import Darwin
/*
print("Int.min = \(Int.min) ads Int.max is \(Int.max)")
print("UInt.min = \(UInt.min) and Int.max is \(UInt.max)")
print("Int8.min = \(Int8.min) and Int8.max is \(Int8.max)")
print("UInt8.min = \(UInt8.min) and UInt8.max is \(UInt8.max)")
print("Double.min = \(DBL_MIN) and Double.max is \(DBL_MAX)")
print("Float.min = \(FLT_MIN) and Float.max is \(FLT_MAX)")*/

let a : Int = 8
let b : Float = 32.9
let c : Double = 12.4

let sumI = a + Int(b) + Int(c)
let sumF = Float(a) + b + Float(c)
let sumD = Double(a) + Double(b) + c

print("Sum of Int = \(sumI), Sum of Float = \(sumF), Sum of Double = \(sumD)")