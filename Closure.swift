import Foundation

//Написать сортировку массива с помощью замыкания, в одну сторону, затем в обратную
//Вывести результат в консоль
var array = [5, 7, 1, 4, 12, 3, 6, 19, 3]

var sortedNumbers = array.sorted(by: {$0 < $1}) //or (by: { (firstNumber: Int, secondNumber: Int) -> Bool return firstNumber < SecondNumber) }
print("The ascending array: \(sortedNumbers)")
sortedNumbers = array.sorted(by: {$0 > $1})
print("The descending array: \(sortedNumbers)")

//Создать метод, который принимает имена друзей, после этого имена положить в массив
//Массив отсортировать по количеству букв в имени
var friends = [String]()

func friendName(name: String){
    friends.append(name)
}

friendName(name: "Tatyana")
friendName(name: "Alexandra")
friendName(name: "Paul")
friendName(name: "Peter")
friendName(name: "Ali")

var sortedFriends = friends.sorted(by: { $0.count < $1.count })

print(sortedFriends)

//Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга
var dictionary = [Int : String]()

//Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение

func getKey(key: Int){
    
    for name in friends where key == name.count{
        dictionary[(name.count)] = name
    }
    print("The key is \(key) and the value is \(dictionary[key])")
}

getKey(key: 4)

//Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль
var integers = [Int]()
var strings = [String]()

func empty(ints: inout[Int], strs: inout[String]){
    if ints.isEmpty{
        ints.append(13)
    }
    else{
        print("Array of integers is not empty")
    }
    if strs.isEmpty{
        strs.append("Hello")
    }
    else{
        print("Array of strings is not empty")
    }
    print("Array of Integers: \(ints)")
    print("Array of Strings: \(strs)")
}

empty(ints: &integers, strs: &strings)