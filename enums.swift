import Foundation
//Создайте по 2 enum с разным типом RawValue
enum Hand : String{
    case Left = "Left Hand"
    case Right = "Right Hand"
}

enum Amount : Int{
    case One = 1
    case Three = 3
    case Five = 5
}

enum Movement{
    case Hands(hand: Hand)
    case Curcles(amount: Amount)
}

//Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж
struct Empolyee{
    enum Gender{
        case Male
        case Female
    }
    enum Age{
        case YoungAdult //18-24
        case Adult //24-35
        case MiddleAged //36-55
        case SeniorCitizen //55+
    }
    enum WorkExperience{
        case Little
        case Middle
        case Great
    }
}

let Boss = Empolyee.WorkExperience.Great

// Создать enum со всеми цветами радуги
enum Rainbow{
    case Red
    case Orange
    case Yellow
    case Green
    case LightBlue
    case Blue
    case Purple
}

// Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple green', 'sun red' и т.д.
func Object (color: Rainbow){
    let objects = ["apple", "sunset", "cup", "fruit", "sky", "car", "parrot"]
    switch color{
        case .Red:
            print("\(objects[0]) is red.")
        case .Orange:
            print("\(objects[1]) is orange.")
        case .Yellow:
            print("\(objects[2]) is yellow.")
        case .Green:
            print("\(objects[3]) is green.")
        case .LightBlue:
            print("\(objects[4]) is light blue.")
        case .Blue:
            print("\(objects[5]) is blue.")
        case .Purple:
            print("\(objects[6]) is purple.")
    }
}

let firstObject = Object(color: .Purple)
let secondObject = Object(color: .Orange)
let thirdObject = Object(color: .LightBlue)

//Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки

enum Scores : String{
    case A
    case B
    case C
    case D
    case E
}

var students = ["Ivanov", "Solokhin", "Chernenko", "Petrova"]

func StudentScore(score: Scores, surname: String){
    switch score{
        case .A:
            print("\(surname), your score is 5.")
        case .B:
            print("\(surname), your score is 4.")
        case .C:
            print("\(surname), your score is 3.")
        case .D:
            print("\(surname), your score is 2.")
        case .E:
            print("\(surname), your score is 1.")
    }
}

let firstStudent = StudentScore(score: .B, surname: students[3])
let secondStudent = StudentScore(score: .D, surname: students[1])

//Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum
enum Cars{
    case Mercedes
    case Audi
    case Porsche
    
    func Garage(car: Cars){
        switch car{
            case .Mercedes:
                print("Wow, you have \(Cars.Mercedes) in your garage.")
            case .Audi:
                print("Wow, you have \(Cars.Audi) in your garage.")
            case .Porsche:
                print("Wow, you have \(Cars.Porsche) in your garage.")
        }
    }
}

let car = Cars.Porsche
car.Garage(car: .Porsche)

