import Foundation

//Реализовать структуру IOSCollection и создать в ней copy on write по типу
struct IOSCollection{
    var number = 14
}

class Ref<T> { //Класс, который хранит ссылку
    var value : T
    init (value: T){
        self.value = value
    } 
}

struct Container <T> { //Контейнер шаблонного типа
    var ref : Ref<T> //хранит ссылку на переменную
    init(value: T){
        self.ref = Ref(value: value) //Для ссылки создает новую ссылку
    }

    var value : T {
        get { //по ссылке получаем значение
            ref.value
        }

        set { //Проверка ссылки на уникальность
            guard(isKnownUniquelyReferenced(&ref)) else { /*Конструкция говорит что на объект имеется одна уникальная ссылка. 
                                                            Если она имеется - меняем ссылке значение
                                                            Иначе - создать новый экземпляр и выйти*/
                ref = Ref(value: newValue)
                return
            }
            ref.value = newValue
        }
    }
}
var numberOne = IOSCollection()
var container1 = Container(value: numberOne)
var container2 = container1


/*Создать протокол *Hotel* с инициализатором, который принимает roomCount, после создать class HotelAlfa 
добавить свойство roomCount и подписаться на этот протокол*/

protocol Hotel {
   init (roomCount : Int)
}

class HotelAlfa : Hotel {
    var roomCount : Int

    required init (roomCount: Int){
        self.roomCount = roomCount
    }
}

var hotel = HotelAlfa(roomCount: 15)
print(hotel.roomCount)

/*Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так, чтобы когда 
мы напишем такую конструкцию 'let diceCoub = 4 diceCoub.numberDice' в консоле мы увидели такую строку - 'Выпало 4 на кубике'*/
protocol GameDice{
    var numberDice : String { get }
}

extension Int : GameDice {
    var numberDice : String {
        return "Выпало \(self) на кубике"
    }
}

let diceCoub = 4
print(diceCoub.numberDice)

/*Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional, создать класс, 
подписать на протокол и реализовать только 1 обязательное свойство*/
@objc protocol PersonInfo {
    var name : String { get }
    @objc optional var age : Int { get }

    func sayHelloTo()
}

class Person : PersonInfo{
    var name : String 
    
    init(name: String){
        self.name = name
    }

    func sayHelloTo(){
        print("Hello, \(name).")
    }
}

let firstPerson = Person(name: "Peter")
firstPerson.sayHelloTo()

/*Создать 2 протокола: со свойствами время, количество кода и функцией writeCode(platform:
 Platform, numberOfSpecialist: Int); и другой с функцией: stopCoding(). 
 Создайте класс: Компания, у которого есть свойства - количество программистов, специализации(ios, android, web)*/
class Platform {
    var specialization : String
    init(specialization: String){
        self.specialization = specialization
    }
}

protocol Begin {
    var time : String { get }
    var amountOfCode : Int { get }

    func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol End {

    func stopCoding()
}

 //Компании подключаем два этих протокола
class Company : Begin, End {
    var numberOfSpecialist : Int
    var amountOfCode : Int
    var time : String
    var platform : Platform

    init(numberOfSpecialist: Int, time: String, amountOfCode: Int, platform: Platform){
        self.numberOfSpecialist = numberOfSpecialist
        self.time = time
        self.amountOfCode = amountOfCode
        self.platform = platform
    }
     /*Задача: вывести в консоль сообщения - 'разработка началась. пишем код <такой-то>' и 
    'работа закончена. Сдаю в тестирование', попробуйте обработать крайние случаи.*/
    func writeCode(platform: Platform, numberOfSpecialist: Int){
        if(numberOfSpecialist <  5 && numberOfSpecialist > 1){
            print("Разработка началась. \(numberOfSpecialist) специалиста должны написать код по специализации \(platform.specialization) на \(amountOfCode) строк за \(time).")
        }
         if(numberOfSpecialist == 1){
            print("Разработка началась. \(numberOfSpecialist) специалист должен написать код по специализации \(platform.specialization) на \(amountOfCode) строк за \(time).")
        }
         if(numberOfSpecialist > 4){
            print("Разработка началась. \(numberOfSpecialist) специалистов должны написать код по специализации \(platform.specialization) на \(amountOfCode) строк за \(time).")
         }
    }

    func stopCoding(){
        print("Работа закончена. Сдано в тестирование.")
    }
}

let platform = Platform(specialization: "android")
let company = Company(numberOfSpecialist: 3, time: "4 дня", amountOfCode: 113, platform: platform)
company.writeCode(platform: company.platform, numberOfSpecialist: company.numberOfSpecialist)
company.stopCoding()